# falcon-node-sensor-helm-chart

## An unofficial helm chart for installing a falcon node sensor with Helm.
All this thing does is install one k8s resource (the falcon node sensor) which requires two variables to be passed in:
```
        - client_id
        - client_secret
```  
    With the defaults being:
```
        - `cloud_region: autodiscover`
        - `node:
             version: ""` #defaults to latest version
```
## Installing with ArgoCD

Input following template under the plugin portion of app.yaml:

```
        env:
        - name: HELM_VALUES
          value: |
            falcon-node-sensor: #name of helm chart
              falconNodeSensor: 
                falcon_api:
                  client_id: <client_id>
                  client_secret: <client_secret>
                  cloud_region: #optional
                node:
                  version: #optional
```

## Installing with Helm

    1. Add this helm repo, update.
    2. helm install --set vars.
    
## Creating a Helm Chart Release
    
    1. Bump the version in Chart.yaml
    2. Package the helm chart into a targz:
```
    helm package --destination ./releases .
```
    3. Publish the chart to Gitlab. Be sure to change falcon-node-sensor-0.1.0.tgz and development with the correct version and channel of the chart.

```
        curl -X POST \
          --form 'chart=@releases/falcon-node-sensor-0.2.0.tgz' \
          --user dimapoperechnyy:$GITLAB_PAT \
          https://gitlab.com/api/v4/projects/55576032/packages/helm/api/development/charts
```


## Happy Helming!
